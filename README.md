# Kisphp Twig Extensions

[![pipeline status](https://gitlab.com/kisphp/twig-extensions/badges/master/pipeline.svg)](https://gitlab.com/kisphp/twig-extensions/-/commits/master)
[![coverage report](https://gitlab.com/kisphp/twig-extensions/badges/master/coverage.svg)](https://gitlab.com/kisphp/twig-extensions/-/commits/master)

## Installation

```bash
composer require kisphp/twig-extensions
```

## Usage

### Create twig Filter

```php
<?php

namespace AppBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;

class DemoFilter extends AbstractTwigFilter
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'demo';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($name) {
            // add here your filter logic
        };
    }
}


```### Create twig Function

```php
<?php

namespace AppBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;

class DemoFunction extends AbstractTwigFunction
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'demo';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($name) {
            // add here your function logic
        };
    }
}

```

### Register new twig extensions

```php
<?php

namespace AppBundle\Twig;

use AppBundle\Twig\Functions\DemoFunction;
use AppBundle\Twig\Filters\DemoFilter;
use Twig\Extension\AbstractExtension;

class TwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            DemoFunction::create(),
        ];
    }

    public function getFilters()
    {
        return [
            DemoFilter::create(),
        ];
    }
}
```

If your function of filter has dependencies then you can instantiate them like this:

```php
public function getFunctions()
{
    $myCustomDependency = 'object, variable, array or what ever you need here';

    return [
        (new DemoFunction($myCustomDependency))->getExtension(),
    ];
}
```
